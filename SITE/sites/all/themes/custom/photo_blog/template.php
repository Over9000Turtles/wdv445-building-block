<?php

function photo_blog_preprocess(&$variables)
{
	$page = $variables['page'];
	$content_class = "";
	
	if (empty($page['left_column']))
	{
		$content_class = "no-left-padding";
	}
	
	
	$variables['photo_blog']['content_class'] = $content_class;
}

?>