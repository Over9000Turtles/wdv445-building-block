<!DOCTYPE html>
<html>
<head>
    <title>Midterm Theme</title>
    <link rel="stylesheet" type="text/css" href="css/layout.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>

<body>
    
    <div class="site-container no-bg no-border">
        <!-- site title -->
        <div class="site-title-container">
            <div class="site-title">
                <p>Midterm Theme</p>
            </div>
        </div>
    </div>
    
    <div class="site-container">
        
        <div class="content-container container">
            <div class="menu-container">
               <?php print theme('links',array('links'=>$main_menu));?>
            </div>
            
            <div class="content clearfix">
                
                <!-- messages will go here -->
                <div id="messages">
                <?php print $messages; ?>
                </div>
                
                <!-- tabs will go here -->
                <div class="tab-container container">
                </div>
                
                <div class="title inner-container">
            	</div>
                
                
                
                <!-- left column region -->
                <div class="left-column column region one-fourth left">
                    
                    <div class="inner">
                         <?php print render($page['left_column']);?>
                    </div>
                </div>
                
                <div class="main-content three-fourths left">
                    
                    <!-- main content -->
                    <div class="title">
                        <h1><?php print $title; ?></h1>
                    </div>
                    
                    <div class="content">
                        <?php print render($page['content']);?>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- footer region -->
        <div class="footer-container container">
            <div class="footer-content inner-container">
               <?php print render($page['footer']); ?>
            </div>
        </div>
        
    </div>
</body>

</html>
